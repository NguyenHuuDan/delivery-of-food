import React, { Component } from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
class Footer extends Component {
    render(){
    return (
        <div>
           <div className="footer">
               <div className="footer-icons">
               <div className="image">
                <img src="logoapp.png"/>
                </div>
                   <div className="footer-dev">               
                    <i class="fa fa-twitter"></i>     
                    <i class="fa fa-facebook-official"></i> 
                    <i class="fa fa-instagram"></i>
               </div>
               <p className="devo">Just type what's on your mind and we'll</p>
               <p className="copy">
                   <i class=" fa fa-copyright"></i>
                   Copyright 2020 Bella Onojei.com
               </p>
               </div>
               </div> 
        </div>
    )
}
}
export default Footer;