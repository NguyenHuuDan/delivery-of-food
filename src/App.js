import React from 'react'
import Header from './components/header';
import Banner from './components/banner';
import Content from './components/content';
import Footer from './components/footer';
function App() {
  return (
    <div >
      <Header/>
      <Banner/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
